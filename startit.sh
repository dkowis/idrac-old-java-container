#!/bin/bash

docker run -it \
  --net host \
  --cpuset-cpus 0 \
  --memory 512mb \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  -e DISPLAY=unix$DISPLAY \
  --device /dev/snd \
  --device /dev/dri \
  -v /dev/shm:/dev/shm \
  --name firefox-esr \
  firefox-esr:latest

