FROM i386/ubuntu:18.04

LABEL maintainer="david.kowis@wnco.com"

# Borrowed heavily from: https://github.com/jessfraz/dockerfiles/blob/master/chrome/stable/Dockerfile
# and: http://blog.michelemattioni.me/2016/02/09/how-to-get-webex-running-on-an-ubuntu-smoothly/
# https://collaborationhelp.cisco.com/article/en-us/9q0glab might be useful

# add the Java8 from oracle
# TODO: change this so I don't need to do it via PPA (but I'm lazy...)
RUN apt-get update && apt-get install -y software-properties-common
RUN add-apt-repository --yes ppa:webupd8team/java \
      && echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections \
      && echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 seen true" | debconf-set-selections \
      && apt install -y oracle-java8-installer \
      && apt clean

# Use the ESR build of firefox for java plorgin
RUN add-apt-repository --yes ppa:mozillateam/ppa \
      && apt clean

# add a user for firefox
RUN groupadd -r firefox \
      && useradd -r -g firefox -G audio,video firefox \
      && mkdir -p /home/firefox/Downloads \
      && chown -R firefox:firefox /home/firefox

RUN \
      apt-get update \
      && apt-get install -y firefox-esr \
      && apt-get clean \
      && mkdir -p /home/firefox/.mozilla/plugins \
      && chown -R firefox:firefox /home/firefox/.mozilla

#Link in the java plugin
RUN ln -vs /usr/lib/jvm/java-8-oracle/jre/lib/i386/libnpjp2.so /home/firefox/.mozilla/plugins/

# update the java security file
COPY java.security /etc/java-8-oracle/security/java.security
RUN mkdir -p /home/firefox/.java/deployment/security
COPY exception.sites /home/firefox/.java/deployment/security/exception.sites
RUN chown -R firefox:firefox /home/firefox

USER firefox
ENTRYPOINT ["firefox-esr"]
CMD ["--profile /profile"]
